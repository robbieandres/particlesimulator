package Particles

import scala.collection.mutable

class BinaryHeap2[A: Manifest](higherPriority: (A, A) => Boolean) {

  private var default: A = _
  private val heap = mutable.ArrayBuffer[A](default)

  def enqueue(o: A) {
    heap += o
    var bubble = heap.length - 1
    while (bubble > 1 && higherPriority(o, heap(bubble / 2))) {
      heap(bubble) = heap(bubble / 2)
      bubble /= 2
    }
    heap(bubble) = o
  }
  def dequeue(): A = {
    val temp = heap(1)
    val a = heap.remove(heap.length - 1)
    if (heap.length > 1) {
      var stone = 1
      var flag = true
      while (stone * 2 < heap.length && flag) {
        var lesserChild = stone * 2
        if (stone * 2 + 1 < heap.length && higherPriority(heap(stone * 2 + 1), heap(lesserChild))) lesserChild += 1
        if (higherPriority(heap(lesserChild), a)) {
          heap(stone) = heap(lesserChild)
          stone = lesserChild
        } else {
          flag = false
        }
      }
      heap(stone) = a
    }
    temp
  }
  def peek: A = heap(1)
  def isEmpty: Boolean = heap.length == 1
}