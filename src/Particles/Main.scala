package Particles

import scala.util.parsing.combinator.JavaTokenParsers
import java.awt.Dimension
import scala.collection.mutable
import swing.BorderPanel.Position._
import java.awt.Graphics2D
import scala.swing.MainFrame
import scala.swing.Panel
import java.awt.image.BufferedImage
import java.awt.Color
import java.awt.geom.Ellipse2D
import scala.swing.Action
import scala.swing.Menu
import scala.swing.MenuBar
import scala.swing.MenuItem
import scala.swing.BorderPanel
import scala.swing.GridPanel
import scala.swing.ComboBox
import scala.swing.Button
import scala.swing.TextField
import scala.swing.TextArea
import scala.swing.Separator
import scala.swing.Swing
import scala.swing.Orientation
import scala.swing.BoxPanel
import scala.swing.Label
import scala.swing.event.EditDone

//cos Force
//  [ -b - sqrt(b^2 - 4ac) ]/ 2a = time they hit
//once you find time make them approach the time
//v1 is velocity of the particle
// particle one particle 2 and time they hit
// tells you all the particles that hit within the time period

object Main extends JavaTokenParsers{

  def dist = distance.text
  def form = formula.text
  var nFormula = ""
  var nDistance = ""
  
  val comboBoxes = new GridPanel(1,4) {
    contents += new Label("")
    contents += new Label("Distance")
    contents += new Label("Formula")
    contents += new Label("")
  }
 
  val formula = new TextField()
  val distance = new TextField()
  val textFields = new GridPanel(1,3) {
    contents += new Label("Enter for both")
    contents += distance
    contents += formula
  }
  
  
  val innerBottom = new BorderPanel {
    layout += textFields -> BorderPanel.Position.Center
    layout += Button("Enter")(changeCode) -> BorderPanel.Position.East
  }
  
  val topBorder = new BorderPanel {
    layout += comboBoxes -> BorderPanel.Position.North
    layout += innerBottom -> BorderPanel.Position.Center
  }
  
  val pa = new Particle(Vect3D(100,150,0),Vect3D(0,0,0),1,10)
  val pb = new Particle(Vect3D(300,300,0),Vect3D(0,0,0),1,20)
  val pc = new Particle(Vect3D(100,250,0),Vect3D(0,0,0),1,30)
  val pd = new Particle(Vect3D(200,75,0),Vect3D(0,0,0),1,50)
  
  val pList = mutable.Buffer[Particle](pa,pb,pc)
  
  val randomX = scala.util.Random.nextInt(300)
  val randomY = scala.util.Random.nextInt(300)
  
  val img = {
    val i = new BufferedImage(500,500,BufferedImage.TYPE_INT_ARGB)
    val g = i.createGraphics
    g.setPaint(Color.black)
    g.fillRect(0,0,i.getWidth, i.getHeight)
    g.setPaint(Color.white)
    g.fill(new Ellipse2D.Double(50,50,20,20))
    i
  } 
  
  def endGame {
    timer.stop
    sys.exit(0)
  }
  
  def changeCode {  //need to make function new force
    nFormula = formula.text
    nDistance = distance.text
  }
  
  val panel = new BorderPanel {
      layout += topBorder -> BorderPanel.Position.North
      layout += new Panel {  //space program should be running in
        override def paint (g:Graphics2D) {
          SimPlot.render(pList, g,size.width,size.height)
        } 
    }-> BorderPanel.Position.Center  
  }
  
  val frame = new MainFrame {
    title = "Random Motion"
    contents = panel 
    menuBar = new MenuBar {
      contents += new Menu("File") {
        contents += new MenuItem(Action("Start")(/*what should you do?*/))
        contents += new Separator //
        contents += new MenuItem(Action("Draw Particle")(/*createParticle*/))
        contents += new MenuItem(Action("End Game")(endGame))
      }
    }
    size = new Dimension(img.getWidth,img.getHeight)
    centerOnScreen
  }
  
  val timer:javax.swing.Timer = new javax.swing.Timer(200,Swing.ActionListener(e => {
    
    panel.repaint
  }))
  
  def main(args:Array[String]) {
    frame.open
    timer.start
    val dt = 0.1
    val gravity = new UserForce(dt)
    val sim = new Simulation(gravity,pList)
    formula.listenTo(formula)   //pulls formula Line
    formula.reactions += {
      case ed:EditDone => 
        val newDist = distance.text
        val newForce = formula.text
    }
    while (true) {
      sim.advance       
      sim.secondAdv(dt)
      sim.search(dt)  
      panel.repaint
    }
  }
}