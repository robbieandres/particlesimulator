package Particles

class ExrpressionParser {

}

object ParseSimulator {

  val ops = "+-*/".toSet

  def eval(distance: String, form: String): Double = {
    evalParse(form.filter(_ != ' '), Map("r" -> distance.toDouble))
  }

  private def evalParse(formula: String, vars: Map[String, Double]): Double = {
    var opLoc = -1
    var parensCount = 0
    var i = formula.length - 1
    while (i > 0) {
      if (formula(i) == '(') {
        //        println("first")
        parensCount += 1
      } else if (formula(i) == ')') {
        //      println("second")
        parensCount -= 1
      } else if (parensCount == 0 && (formula(i) == '+' || formula(i) == '-' && !ops.contains(formula(i - 1)))) {
        //    println("third")
        opLoc = i
        i = -1
      } else if (parensCount == 0 && opLoc == -1 && (formula(i) == '*' || formula(i) == '/')) {
        //     println("fourth")
        opLoc = i
      }
      i -= 1
    }
    if (opLoc < 0) {
      println("check")
      if (formula(0) == '(') {
        evalParse(formula.trim.substring(1, formula.length - 1), vars)
      } else if (formula.trim.startsWith("-")) {
        println("see negative")
        -1 * (evalParse(formula.trim.substring(1), vars))
      } else if (vars.contains(formula)) vars(formula)
      else if (formula.trim.startsWith("sqrt")) {
        math.sqrt(evalParse(formula.trim.substring(5, formula.length - 1), vars))
      } else if (vars.contains(formula)) {
        vars(formula)
      } else if (formula.trim.startsWith("sin")) {
        math.sin(evalParse(formula.trim.substring(4, formula.length - 1), vars))
      } else if (formula.trim.startsWith("cos")) {
        math.cos(evalParse(formula.trim.substring(4, formula.length - 1), vars))
      } else formula.toDouble
    } else {
      println("what")
      formula(opLoc) match {
        case '+' => evalParse(formula.take(opLoc), vars) + evalParse(formula.drop(opLoc + 1), vars)
        case '-' => evalParse(formula.take(opLoc), vars) - evalParse(formula.drop(opLoc + 1), vars)
        case '*' => evalParse(formula.take(opLoc), vars) * evalParse(formula.drop(opLoc + 1), vars)
        case '/' => evalParse(formula.take(opLoc), vars) / evalParse(formula.drop(opLoc + 1), vars)
      }
    }
  }
}