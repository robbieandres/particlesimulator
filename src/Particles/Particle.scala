package Particles

import java.awt.Graphics2D
import java.awt.geom.Ellipse2D
import java.awt.Color

class Particle (private var mx:Vect3D, private var mv:Vect3D, val mass:Double,val radius:Double){
  def x = mx
  def v = mv
  def v_= (nv: Vect3D) = mv = nv
  def r = radius
  def m = mass
  def accelerate(a:Vect3D, dt:Double):Unit = {
    mv += a*dt
  }
  
  def step(dt:Double):Unit = {
    mx += v*dt
  }
  
  def advance(a:Vect3D, dt:Double):Unit = {
    accelerate(a,dt)
    step(dt)
  }
}