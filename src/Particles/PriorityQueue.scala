package Particles

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class PriorityQueue [A:Manifest](higherPriority:(A,A)=> Boolean)  {

  private var heap = new ArrayBuffer[A](10)    //array buffer
  private var back = 1
  
  def dequeue(): A = {
    val tmp = heap(1)
    val a = heap.remove(heap.length - 1)
    var stone = 1
    var flag = true //keep going while flag is true
    while (stone * 2 < back && flag) {
      // until we find a resting place (we got to a node with no children && both children are lower priority than what is settling down  
      var priorityChild = stone * 2 //should we go lower
      if (stone * 2 + 1 < back && higherPriority(heap(stone * 2 + 1), heap(priorityChild))) //if there is a second child && if the second child has a higherPriority than the 1st child
        priorityChild += 1 //we make the 2nd child the 1st child
      if (higherPriority(heap(priorityChild), a)) { //if priorityChild is higher priority than the value being synced down
        heap(stone) = heap(priorityChild) //move priority child up
        stone = priorityChild // we take and set stone = priority child
      } else { //if priority value is not higher than temp value
        flag = false //this makes while loop stop because found higher priority value is not higher than temp
      }
    }
    heap(stone) = a //store this in 
    tmp //return value we found initially
  }
  
  def enqueue(o:A) = {
    var bubble = heap.length  //should always start at the bottom 
    while(bubble>1 && higherPriority(o,heap(bubble/2))) { //bubble is not in root & object were inserting has higher value than parent
      heap(bubble) = heap(bubble/2)  //move bubble parents down
      bubble /= 2  //move bubble up to parents position  bubble is divided by 2
    }
    heap(bubble) = o   //if condition is false store value in bubble
  }
  
  def isEmpty:Boolean = back ==1
    
  
  def peek(o:A):A = heap(1)
}