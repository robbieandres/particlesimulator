package Particles

import scala.swing.BorderPanel

import scala.collection.mutable
import java.awt.Graphics2D
import java.awt.Color
import java.awt.geom.Ellipse2D

object SimPlot extends BorderPanel {

  def getValue(p: Particle, which:Int):Double = {
    ???
  }
  
  def clearPot(): Unit = {
    ???
  }
  
  def render(mParticles: mutable.Buffer[Particle], g:Graphics2D, width:Int, height:Int):Unit = {
    g.setPaint(Color.black)
    g.fillRect(0,0,width,height)       
    
    for(e <- 0 until mParticles.length) {
      g.setPaint(Color.cyan)
      //draws on a square and have to 2*r for it to reach entire area of the box
      g.fill(new Ellipse2D.Double(mParticles(e).x.x - mParticles(e).radius,mParticles(e).x.y - mParticles(e).radius,2*mParticles(e).radius,2* mParticles(e).radius))
    }
 
  }
}