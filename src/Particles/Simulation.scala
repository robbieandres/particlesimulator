package Particles

import scala.collection.mutable

case class PotentialCollision(time: Double, p1: Particle, p2: Particle) {

}

class Simulation(val force: TimeStepForce, private val mParticles: mutable.Buffer[Particle]) {
  val dt = 0.01

  def p(index: Int) = mParticles(index)

  def addParticle(p: Particle): Unit = {
    mParticles += p
  }

  def numParticles: Int = {
    mParticles.size
  }

  def iterator: Iterator[Particle] = {
    mParticles.iterator
  }

  def advance(): Unit = { //calls force to calculate acceleration
    val acc = force.calcAccelerations(mParticles.toIndexedSeq)
    for (i <- 0 until acc.length) {
      mParticles(i).advance(acc(i), dt)
    }
  }

  def secondAdv(dt: Double): Unit = {
    mParticles.foreach(_.step(dt))
  }

  def collisionTime(p1: Particle, p2: Particle): Double = {
    val dx = p1.x - p2.x
    val dv = p1.v - p2.v
    val a = dv dot dv
    val b = 2 * (dx dot dv)
    val c = (dx dot dx) - (p1.r + p2.r) * (p1.r + p2.r) //d^2(t)
    if (b * b - 4 * a * c < 0) -1.0
    else {
      (-b - math.sqrt(b * b - 4 * a * c)) / (2 * a) //distance
    }
  }

  def collision(p1: Particle, p2: Particle, t: Double): Unit = {
    //advance particles to time of that collision
    p1.step(t) //brings particle to the collision time
    p2.step(t)
    //calculates new velocities after impact
    val dx = p1.x - p2.x //deltas
    val dv = p1.v - p2.v
    val dnormal = dx / dx.mag
    val cmx = (p1.x * p1.m + p2.x * p2.m) / (p1.m + p2.m) // center of masses
    val cmv = (p1.v * p1.m + p2.v * p2.m) / (p1.m + p2.m)
    val cmv1 = p1.v - cmv //velocities relative to center of mass
    val cmv2 = p2.v - cmv
    val cmv1perp1 = dnormal * (cmv1 dot dnormal) //calculate velocity component along direction of impact
    val impactv1 = p1.v - (cmv1perp1 * 2)
    val cmv1perp2 = dnormal * (cmv2 dot dnormal)
    val impactv2 = p2.v - (cmv1perp2 * 2)
    p1.v = impactv1
    p2.v = impactv2
    //position doesn’t change at the impact,just the velocity. 
    //need to keep track of what “time” every particle is at.
  }

  //identify collision & the queue
  def search(dt: Double): Unit = {
    val pq = new BinaryHeap2[PotentialCollision](_.time < _.time)  //
    for (i <- 0.00 until dt by 0.01) {
      for (i <- 0 until mParticles.length-1; j <- i until mParticles.length) { //
        val ct = collisionTime(p(i), p(j))
     //   println("collision time:"+ct)  //countdown is working fine
        if (0 <= ct && ct <= dt) { //if they experience collision
          pq.enqueue(PotentialCollision(ct, mParticles(i), mParticles(j))) // add potential collision to priority queue
 //         println("Potential Collision enterred in queue: "+pq)
        }
        //pull off first potential collision, process it, remove collisions in queue after process
        // research for other collisions before count reaches dt
        //whatever you find add to queue and repeat process above
        while (!pq.isEmpty) {
          val pc = pq.dequeue()
  //        println("Dequeue Potential Collision "+pc)
          collision(pc.p1, pc.p2, pc.time)  //error is in collision
        }
      }
    }
  }
}