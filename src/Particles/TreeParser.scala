package Particles

class TreeParser(val formula: String) extends Serializable {
  private val root = TreeParser.parse(formula)
  def eval(vars: Map[String, Double]): Double = root eval vars
  override def toString = formula
}

object TreeParser {
  val ops = "+-*/".toSet
  
  def eval(form: String, vars: Map[String, Double] = null): Double = {
    val root = parse(form.filter(_ != ' '))
    root.eval(vars)
  }
  
  private def parse(f: String): Node = {
    var opLoc = -1
    var parensCount = 0
    var i = f.length - 1
    while (i > 0) {
      if (f(i) == '(') parensCount += 1
      else if (f(i) == ')') parensCount -= 1
      else if (parensCount == 0 && (f(i) == '+' || f(i) == '-' && !ops.contains(f(i - 1)))) {
        opLoc = i
        i = -1
      } else if (parensCount == 0 && opLoc == -1 && (f(i) == '*' || f(i) == '/')) {
        opLoc = i
      }
      i -= 1
    }
    if (opLoc < 0) {
      if (f(0) == '(') {
        parse(f.substring(1, f.length - 1))
      } else if (f.startsWith("sin(")) {
        new SingleOpNode(parse(f.substring(4, f.length - 1)), math.sin)
      } else if (f.startsWith("cos(")) {
        new SingleOpNode(parse(f.substring(4, f.length - 1)), math.cos)
      } else if (f.startsWith("tan(")) {
        new SingleOpNode(parse(f.substring(4, f.length - 1)), math.tan)
      } else if (f.startsWith("sqrt(")) {
        new SingleOpNode(parse(f.substring(5, f.length - 1)), math.sqrt)
      } else try {
        new NumberNode(f.toDouble)
      } catch {
        case ex: NumberFormatException => new VariableNode(f)
      }
    } else {
      f(opLoc) match {
        case '+' => new BinaryOpNode(parse(f.take(opLoc)), parse(f.drop(opLoc + 1)),
          _ + _)
        case '-' => new BinaryOpNode(parse(f.take(opLoc)), parse(f.drop(opLoc + 1)),
          _ - _)
        case '*' => new BinaryOpNode(parse(f.take(opLoc)), parse(f.drop(opLoc + 1)),
          _ * _)
        case '/' => new BinaryOpNode(parse(f.take(opLoc)), parse(f.drop(opLoc + 1)),
          _ / _)
      }
    }
  }
  private trait Node extends Serializable {
    def eval(vars: Map[String, Double]): Double
  }
  private case class NumberNode(num: Double) extends Node {
    def eval(vars: Map[String, Double]): Double = num
  }
  private case class VariableNode(name: String) extends Node {
    def eval(vars: Map[String, Double]): Double = vars get name getOrElse 0.0
  }
  private case class BinaryOpNode(left: Node, right: Node, op: (Double, Double) => Double) extends Node {
    def eval(vars: Map[String, Double]): Double = op(left eval vars, right eval vars)
  }
  private case class SingleOpNode(arg: Node, op: Double => Double) extends Node {
    def eval(vars: Map[String, Double]): Double = op(arg eval vars)
  }
}