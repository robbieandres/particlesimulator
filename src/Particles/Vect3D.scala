//v*3 = v.*3
//problem with 3*v = 3.*v  there is no .v

package Particles

case class Vect3D (val x:Double, val y:Double, val z:Double){
    def +(v:Vect3D) = Vect3D(x+v.x,y+v.y,z+v.z)
    def -(v:Vect3D) = Vect3D(x-v.x,y-v.y,z-v.z)
    def *(b:Double) = new Vect3D(x*b, y*b, z*b)
    def /(b:Double) = new Vect3D(x/b, y/b, z/b)
    def mag = Math.sqrt(x*x+y*y+z*z)
    def dot(v:Vect3D) = x*v.x+y*v.y+z*v.z
    def cross(v:Vect3D) = Vect3D(
        y*v.z-z*v.y,
        z*v.x-x*v.z,
        x*v.y-y*v.x
    )
}